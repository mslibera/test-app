<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CcpsCore\HomeController@index')->name('home');
Route::get('home', 'CcpsCore\HomeController@index');

Route::group(['prefix' => 'account'], function() {
    Route::get('/', 'CcpsCore\AccountController@index')->name('account');
    Route::get('/settings', 'CcpsCore\AccountController@settings')->name('account.settings');
    Route::get('/profile', 'CcpsCore\AccountController@profile')->name('profile.show');
    Route::get('/profile/edit', 'CcpsCore\AccountController@editProfile')->name('profile.edit');
    Route::patch('/profile/', 'CcpsCore\AccountController@updateProfile')->name('profile.update');
});

// Routes from Modules
$ccpsModules = collect(config('ccps.modules'));
$adminModules = $ccpsModules->where('parent', 'admin')->where('use_custom_routes', false);

Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function() {
    Route::get('/', 'CcpsCore\AdminController@index')->name('admin');

    foreach(config('ccps.modules') as $name => $module) {
        if (!$module['use_custom_routes'] && $module['parent'] == 'admin') {
            require_once(base_path('vendor/' . $module['package'] . '/src/routes/' . $name . '.php'));
        }
    }
});

foreach(config('ccps.modules') as $name => $module) {
    if (!$module['use_custom_routes'] && empty($module['parent'])) {
        require_once(base_path('vendor/' . $module['package'] . '/src/routes/' . $name . '.php'));
    }
}

// For local auth
Auth::routes();

// Socialite - User Account OAuth Routes
Route::get('auth/{provider}', 'CcpsCore\AuthController@redirectToProvider')->name('oauth');
Route::get('auth/{provider}/callback', 'CcpsCore\AuthController@handleProviderCallback')->name('oauth.callback');
