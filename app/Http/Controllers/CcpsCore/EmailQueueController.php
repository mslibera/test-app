<?php

namespace App\Http\Controllers\CcpsCore;

use Uncgits\Ccps\Controllers\EmailQueueController as BaseController;

class EmailQueueController extends BaseController
{
    public function __construct() {
        parent::__construct();
    }
}
