<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // Database errprs
        'App\Events\CcpsCore\UncaughtQueryException' => [
            'App\Listeners\CcpsCore\LogUncaughtQueryException',
        ],

        // Email
        'Illuminate\Mail\Events\MessageSent' => [
            'App\Listeners\CcpsCore\LogSentEmail',
        ],

        // Cron Jobs
        'App\Events\CcpsCore\CronjobStarted' => [
            'App\Listeners\CcpsCore\PreCronjob',
        ],
        'App\Events\CcpsCore\CronjobFinished' => [
            'App\Listeners\CcpsCore\PostCronjob',
        ],

        // Login / Logout
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\CcpsCore\LogLoginData',
        ],
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\CcpsCore\LogLogoutData',
        ],
        'Illuminate\Auth\Events\Failed' => [
            'App\Listeners\CcpsCore\LogFailedLoginData',
        ],

        // ACL
        'App\Events\CcpsCore\AclChanged' => [
            '\App\Listeners\CcpsCore\LogAclChange'
        ],

        // Cache
        'App\Events\CcpsCore\CacheCleared' => [
            'App\Listeners\CcpsCore\LogCacheClear',
        ],

        // Config
        'App\Events\CcpsCore\ConfigUpdated' => [
            'App\Listeners\CcpsCore\LogConfigUpdate',
        ],

        // Backups
        'Spatie\Backup\Events\BackupManifestWasCreated' => [
            'App\Listeners\CcpsCore\LogBackupManifestInfo',
        ],

        'Spatie\Backup\Events\BackupZipWasCreated' => [
            'App\Listeners\CcpsCore\LogBackupZipCreationInfo',
        ],

        'Spatie\Backup\Events\BackupWasSuccessful' => [
            'App\Listeners\CcpsCore\LogSuccessfulBackup',
        ],

        'Spatie\Backup\Events\BackupHasFailed' => [
            'App\Listeners\CcpsCore\LogFailedBackup',
        ],

        'Spatie\Backup\Events\CleanupWasSuccessful' => [
            'App\Listeners\CcpsCore\LogSuccessfulCleanup'
        ],

        'Spatie\Backup\Events\CleanupHasFailed' => [
            'App\Listeners\CcpsCore\LogFailedCleanup'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
