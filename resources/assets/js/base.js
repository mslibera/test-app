// Bootstrap
require('./bootstrap');

// user functions
window.str_slug = function (val, char) {
    if (char === undefined) {
        char = '-';
    }
    var replacedAts = val.replace(/@/g, "at");
    var noCharsName = replacedAts.trim().replace(/[^a-z0-9\. ]/gi, "");
    return noCharsName.replace(/ /g, char).toLowerCase();
};
