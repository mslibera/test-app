@extends('layouts.wrapper', [
    'pageTitle' => '403'
])

@section('content')
    <h2>403 - Forbidden</h2>
    <div class="alert alert-danger">
        <i class="fa fa-exclamation-triangle"></i>
        <strong>Error:</strong> You do not have permission to view this page.
    </div>
    <p><a href="{{ back()->getTargetUrl() }}">Go Back</a></p>

    <p>{{ $exception->getMessage() }}</p>
@endsection()