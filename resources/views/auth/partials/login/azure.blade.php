<div class="panel panel-default">
    <div class="panel-heading">Azure</div>
    <div class="panel-body text-center">
        <a href="{{ route('oauth', ['provider' => 'azure']) }}" class="btn btn-primary btn-success" title="Login with Azure">
            <i class="fa fa-2x fa-windows"></i> Azure
        </a>
    </div>
</div>