@extends('layouts.wrapper', [
    'pageTitle' => 'Home'
])

@section('content')
    <h1>{{ config('app.name') }}</h1>
    <div class="row">
        @if($user)
            @role('admin')
                @include('components.panel-nav', [
                    'url' => route('admin'),
                    'fa' => 'key',
                    'title' => 'Administrator'
                ])
            @endrole

            @foreach($modules as $key => $module)
                @if(empty($module['parent']))
                    @permission($module['required_permissions'])
                    @include('components.panel-nav', [
                        'url' => (empty($module['parent']) ? '' : $module['parent']) . '/' . $module['index'],
                        'fa' => $module['icon'],
                        'title' => $module['title']
                    ])
                    @endpermission
                @endif
            @endforeach

        @else
            <div class="col-md-12">
                <p>You are not logged in - please <a href="{{ route('login') }}">log in</a> to proceed.</p>
                <p>Well lookit this. It's my app that I built off of ccps-core.</p>
                <p>I've now changed it twice.</p>
            </div>
        @endif
    </div>
@endsection