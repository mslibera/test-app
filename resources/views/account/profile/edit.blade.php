@extends('layouts.wrapper', [
    'pageTitle' => 'Edit Profile'
])

@section('content')
    {!! Breadcrumbs::render('profile.edit') !!}

    <h1>Edit Profile</h1>

    <form action="{{ route('profile.update') }}" method="POST">
        <div class="form-group {{ empty($errors->get('first_name')) ? "" : " has-error" }}">
            <label for="first_name">First Name:*</label>
            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ old('first_name', $userToEdit->first_name) }}">
        </div>
        <div class="form-group {{ empty($errors->get('last_name')) ? "" : " has-error" }}">
            <label for="last_name">Last Name:*</label>
            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ old('last_name', $userToEdit->last_name) }}">
        </div>

        @if($userToEdit->provider == 'local')
            <div class="form-group {{ empty($errors->get('email')) ? "" : " has-error" }}">
                <label for="email">Email address:*</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Name" value="{{ old('email', $userToEdit->email) }}">
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" value="1" name="reset_user_password"
                    @if(old('reset_user_password') !== null)
                        checked
                    @endif
                    >
                    Reset your password?
                </label>
                <p class="help-block">Checking this box will invalidate your current password and you a password reset email.</p>
            </div>
        @else
            <p><em>Note: your email address and password cannot be changed because you registered using a third party provider: {{ ucwords($userToEdit->provider) }}.</em></p>
        @endif

        {{ csrf_field() }}
        {{ method_field("PATCH") }}

        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
        <a href="/admin/users" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
    </form>

@endsection()